# go-phoneBook

Simple go PhoneBook webapp using GORM with postgres

## Prerequisites
* go >= 1.14
* postgres db instance

## How to run
* clone this repo 

`$ git clone https://gitlab.com/infralighter/go-phonebook`
* fill .env variables(db name, host, port, credentials)
* run `$ go build .`
* run `$ ./phoneBook`
* enjoy curling paths `/api/contacts`, `/api/contacts/{id}`
