package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
	"os"
	"phoneBook/controllers"
)

func main() {

	router := mux.NewRouter()

	router.HandleFunc("/api/contacts", controllers.GetContacts).Methods("GET")
	router.HandleFunc("/api/contacts/{id}", controllers.GetContact).Methods("GET")
	router.HandleFunc("/api/contacts", controllers.CreateContact).Methods("POST")
	router.HandleFunc("/api/contacts/{id}", controllers.UpdateContact).Methods("PUT")
	router.HandleFunc("/api/contacts/{id}", controllers.DeleteContact).Methods("DELETE")

	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}

	fmt.Println("Listening on " + port)

	err := http.ListenAndServe(":"+port, router)
	if err != nil {
		fmt.Println(err)
	}
}
