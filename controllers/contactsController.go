package controllers

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"net/http"
	"phoneBook/models"
	u "phoneBook/utils"
	"strconv"
)

var GetContacts = func(w http.ResponseWriter, r *http.Request) {

	data := models.GetContacts()
	json.NewEncoder(w).Encode(data)
}

var GetContact = func(w http.ResponseWriter, r *http.Request)  {

	id, err := strconv.ParseUint(mux.Vars(r)["id"], 10, 32)
	if err != nil {
		w.WriteHeader(400)
		data := u.Message(false, "Invalid id")
		u.Respond(w, data)
		return
	}

	u.Respond(w, models.GetContact(id))
}

var CreateContact = func(w http.ResponseWriter, r *http.Request) {

	contact := &models.Contact{}

	err := json.NewDecoder(r.Body).Decode(contact)
	if err != nil {
		w.WriteHeader(400)
		data := u.Message(false, "Error decoding request body")
		u.Respond(w, data)
		return
	}

	response := contact.Create()
	u.Respond(w, response)
}

var UpdateContact = func(w http.ResponseWriter, r *http.Request) {

	id, err := strconv.ParseUint(mux.Vars(r)["id"], 10, 32)
	if err != nil {
		w.WriteHeader(400)
		data := u.Message(false, "Invalid id")
		u.Respond(w, data)
		return
	}

	contact := &models.Contact{}

	err = json.NewDecoder(r.Body).Decode(contact)
	if err != nil {
		u.Respond(w, u.Message(false, "Error decoding request body"))
		return
	}

	response := contact.Update(id)
	u.Respond(w, response)
}

var DeleteContact = func(w http.ResponseWriter, r *http.Request) {

	id, err := strconv.ParseUint(mux.Vars(r)["id"], 10, 32)
	if err != nil {
		w.WriteHeader(400)
		data := u.Message(false, "Invalid id")
		u.Respond(w, data)
		return
	}

	contact := &models.Contact{}

	response := contact.Delete(id)
	u.Respond(w, response)
}
