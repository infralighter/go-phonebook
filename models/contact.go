package models

import (
	"fmt"
	u "phoneBook/utils"
)

type Contact struct {
	ID    uint64 `json:"id"`
	Name  string  `json:"name"`
	Phone string  `json:"phone"`
}

func (contact *Contact) Validate() (map[string]interface{}, bool) {
	if contact.Name == "" {
		return u.Message(false, "Contact name missing"), false
	}

	if contact.Phone == "" {
		return u.Message(false, "Phone number missing"), false
	}

	return u.Message(true, "validation successful"), true
}

func GetContact(id uint64) map[string]interface{} {

	contact := &Contact{}
	err := GetDB().Table("contacts").Where("id = ?", id).First(contact).Error
	if err != nil {
		fmt.Println(err)
		return u.Message(false, err.Error())
	}

	return makeResponse(contact)
}

func GetContacts() map[string]interface{} {

	contacts := make([]*Contact, 0)
	err := GetDB().Table("contacts").Find(&contacts).Error
	if err != nil {
		fmt.Println(err)
		return u.Message(false, err.Error())
	}

	response := u.Message(true, "ok")
	response["data"] = contacts
	return response
}

func (contact *Contact) Create() map[string]interface{} {

	if _, ok := contact.Validate(); !ok {
		return nil
	}

	GetDB().Create(contact)

	return makeResponse(contact)
}

func (contact *Contact) Update(id uint64) map[string]interface{} {
	if resp, ok := contact.Validate(); !ok {
		return resp
	}

	old := &Contact{}
	err := GetDB().Table("contacts").Where("id = ?", id).First(old).Error
	if err != nil {
		fmt.Println(err)
		return u.Message(false, err.Error())
	}

	GetDB().Model(old).Updates(&contact)
	return makeResponse(old)
}

func (contact *Contact) Delete(id uint64) map[string]interface{} {

	old := &Contact{}
	err := GetDB().Table("contacts").Where("id = ?", id).First(old).Error
	if err != nil {
		fmt.Println(err)
		return u.Message(false, err.Error())
	}
	GetDB().Delete(old)
	return makeResponse(old)
}

func makeResponse(contact *Contact) map[string]interface{} {
	response := u.Message(true, "ok")
	response["contact"] = contact
	return response
}
