package models

import (
	"fmt"
	. "github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/joho/godotenv"
	"os"
)

var db *DB

func init() {

	// load env
	e := godotenv.Load()
	if e != nil {
		fmt.Println(e)
	}

	//read env
	dbType := os.Getenv("db_type")
	dbHost := os.Getenv("db_host")
	dbPort := os.Getenv("db_port")
	dbName := os.Getenv("db_name")
	dbUser := os.Getenv("db_user")
	dbPassword := os.Getenv("db_pass")

	// connect to db cluster
	dbUri := fmt.Sprintf(
		"host=%s port=%s dbname=%s user=%s password=%s sslmode=disable",
		dbHost, dbPort, dbName, dbUser, dbPassword)

	conn, err := Open(dbType, dbUri)

	if err != nil {
		fmt.Println(err)
	}

	db = conn
	db.LogMode(true)
	db.Debug().AutoMigrate(&Contact{})
}

func GetDB() *DB {
	return db
}
